package com.framework.auth.config;

import com.framework.common.config.web.DefaultPasswordConfig;
import org.springframework.context.annotation.Configuration;

/**
 * @author xie jianchu
 * @date 2019/1/2
 */
@Configuration
public class PasswordConfig extends DefaultPasswordConfig {
}
