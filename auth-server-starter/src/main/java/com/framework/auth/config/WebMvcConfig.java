package com.framework.auth.config;

import com.framework.auth.properties.SecurityProperties;
import com.framework.auth.properties.TokenStoreProperties;
import com.framework.common.config.web.DefaultWebMvcConfig;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({SecurityProperties.class, TokenStoreProperties.class})
public class WebMvcConfig extends DefaultWebMvcConfig {
}
