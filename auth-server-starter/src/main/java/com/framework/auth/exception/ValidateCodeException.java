package com.framework.auth.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 验证码异常
 */
public class ValidateCodeException extends AuthenticationException {

    private static final long serialVersionUID = -187172372433433825L;

    public ValidateCodeException(String msg) {
        super(msg);
    }
}
