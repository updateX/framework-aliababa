package com.framework.auth.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.framework.auth.model.Client;
import com.framework.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ClientMapper extends BaseMapper<Client> {

    List<Client> findList(Page<Client> page, @Param("params") Map<String, Object> params);
}
