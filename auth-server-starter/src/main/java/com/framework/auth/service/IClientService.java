package com.framework.auth.service;

import com.framework.auth.model.Client;
import com.framework.common.base.BaseIService;
import com.framework.common.model.PageResult;
import com.framework.common.model.R;

import java.util.Map;

public interface IClientService extends BaseIService<Client> {

    R saveClient(Client clientDto) throws Exception;

    /**
     * 查询应用列表
     *
     * @param params
     * @param isPage 是否分页
     * @return
     */
    PageResult<Client> listClient(Map<String, Object> params, boolean isPage);

    void delClient(String id);

    Client loadClientByClientId(String clientId);
}
