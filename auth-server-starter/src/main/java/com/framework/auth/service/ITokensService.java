package com.framework.auth.service;

import com.framework.auth.model.TokenVo;
import com.framework.common.model.PageResult;

import java.util.Map;

public interface ITokensService {
    /**
     * 查询token列表
     *
     * @param params   请求参数
     * @param clientId 应用id
     */
    PageResult<TokenVo> listTokens(Map<String, Object> params, String clientId);
}
