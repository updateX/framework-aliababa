package com.framework.admin.mapper;

import com.framework.admin.entity.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统资讯公告信息 Mapper 接口
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Mapper
public interface NoticeMapper extends BaseMapper<Notice> {

}
