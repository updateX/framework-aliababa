package com.framework.admin.service.impl;

import com.framework.admin.entity.WebConfig;
import com.framework.admin.mapper.WebConfigMapper;
import com.framework.admin.service.IWebConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 网站配置信息 服务实现类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Service
public class WebConfigServiceImpl extends ServiceImpl<WebConfigMapper, WebConfig> implements IWebConfigService {

}
