package com.framework.admin.mapper;

import com.framework.admin.entity.SysRolePrivilegeUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户权限配置 Mapper 接口
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Mapper
public interface SysRolePrivilegeUserMapper extends BaseMapper<SysRolePrivilegeUser> {

}
