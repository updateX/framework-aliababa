package com.framework.admin.entity;

import java.time.LocalDateTime;
import com.framework.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 工单记录
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WorkIssue extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id(提问用户id)
     */
    private Long userId;

    /**
     * 回复人id
     */
    private Long answerUserId;

    /**
     * 回复人名称
     */
    private String answerName;

    /**
     * 工单内容
     */
    private String question;

    /**
     * 回答内容
     */
    private String answer;

    /**
     * 状态：1-待回答；2-已回答；
     */
    private Boolean status;

    /**
     * 修改时间
     */
    private LocalDateTime lastUpdateTime;

    /**
     * 创建时间
     */
    private LocalDateTime created;


}
