package com.framework.admin.entity;

import java.time.LocalDateTime;
import com.framework.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 网站配置信息
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WebConfig extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 分组, LINK_BANNER ,WEB_BANNER
     */
    private String type;

    /**
     * 名称
     */
    private String name;

    /**
     * 值
     */
    private String value;

    /**
     * 权重
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private LocalDateTime created;

    /**
     * 超链接地址
     */
    private String url;

    /**
     * 是否使用 0 否 1是
     */
    private Boolean status;


}
