package com.framework.admin.entity;

import java.time.LocalDateTime;
import com.framework.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统资讯公告信息
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Notice extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 标题
     */
    private String title;

    /**
     * 简介
     */
    private String description;

    /**
     * 作者
     */
    private String author;

    /**
     * 文章状态
     */
    private Integer status;

    /**
     * 文章排序，越大越靠前
     */
    private Integer sort;

    /**
     * 内容
     */
    private String content;

    /**
     * 最后修改时间
     */
    private LocalDateTime lastUpdateTime;

    /**
     * 创建日期
     */
    private LocalDateTime created;


}
