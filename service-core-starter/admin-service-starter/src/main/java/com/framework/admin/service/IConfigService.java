package com.framework.admin.service;

import com.framework.admin.entity.Config;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 平台配置信息 服务类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
public interface IConfigService extends IService<Config> {

}
