package com.framework.admin.entity;

import java.time.LocalDateTime;
import com.framework.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统日志
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysUserLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 组
     */
    private String group;

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 日志类型 1查询 2修改 3新增 4删除 5导出 6审核
     */
    private Integer type;

    /**
     * 方法
     */
    private String method;

    /**
     * 参数
     */
    private String params;

    /**
     * 时间
     */
    private Long time;

    /**
     * IP地址
     */
    private String ip;

    /**
     * 描述
     */
    private String description;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private LocalDateTime created;


}
