package com.framework.admin.mapper;

import com.framework.admin.entity.SysUserLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统日志 Mapper 接口
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Mapper
public interface SysUserLogMapper extends BaseMapper<SysUserLog> {

}
