package com.framework.admin.service.impl;

import com.framework.admin.entity.SysUserLog;
import com.framework.admin.mapper.SysUserLogMapper;
import com.framework.admin.service.ISysUserLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统日志 服务实现类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Service
public class SysUserLogServiceImpl extends ServiceImpl<SysUserLogMapper, SysUserLog> implements ISysUserLogService {

}
