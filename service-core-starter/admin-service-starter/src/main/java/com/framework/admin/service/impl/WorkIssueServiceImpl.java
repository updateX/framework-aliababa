package com.framework.admin.service.impl;

import com.framework.admin.entity.WorkIssue;
import com.framework.admin.mapper.WorkIssueMapper;
import com.framework.admin.service.IWorkIssueService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 工单记录 服务实现类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Service
public class WorkIssueServiceImpl extends ServiceImpl<WorkIssueMapper, WorkIssue> implements IWorkIssueService {

}
