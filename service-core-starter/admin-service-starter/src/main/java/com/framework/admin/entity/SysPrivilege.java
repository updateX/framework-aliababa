package com.framework.admin.entity;

import java.time.LocalDateTime;
import com.framework.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 权限配置
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysPrivilege extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 所属菜单Id
     */
    private Long menuId;

    /**
     * 功能点名称
     */
    private String name;

    /**
     * 功能描述
     */
    private String description;

    private String url;

    private String method;

    /**
     * 创建人
     */
    private Long createBy;

    /**
     * 修改人
     */
    private Long modifyBy;

    /**
     * 创建时间
     */
    private LocalDateTime created;

    /**
     * 修改时间
     */
    private LocalDateTime lastUpdateTime;


}
