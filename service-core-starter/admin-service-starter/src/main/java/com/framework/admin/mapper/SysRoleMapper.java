package com.framework.admin.mapper;

import com.framework.admin.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
