package com.framework.admin.entity;

import java.time.LocalDateTime;
import com.framework.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 平台配置信息
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Config extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 配置规则类型
     */
    private String type;

    /**
     * 配置规则代码
     */
    private String code;

    /**
     * 配置规则名称
     */
    private String name;

    /**
     * 配置规则描述
     */
    private String desc;

    /**
     * 配置值
     */
    private String value;

    /**
     * 创建时间
     */
    private LocalDateTime created;


}
