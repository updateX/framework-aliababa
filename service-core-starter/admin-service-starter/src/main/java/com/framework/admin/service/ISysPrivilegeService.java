package com.framework.admin.service;

import com.framework.admin.entity.SysPrivilege;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限配置 服务类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
public interface ISysPrivilegeService extends IService<SysPrivilege> {

}
