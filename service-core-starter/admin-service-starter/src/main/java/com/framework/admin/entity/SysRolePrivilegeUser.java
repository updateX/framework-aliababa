package com.framework.admin.entity;

import com.framework.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户权限配置
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysRolePrivilegeUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 角色Id
     */
    private Long roleId;

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 权限Id
     */
    private Long privilegeId;


}
