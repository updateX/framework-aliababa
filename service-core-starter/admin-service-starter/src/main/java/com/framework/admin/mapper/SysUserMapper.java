package com.framework.admin.mapper;

import com.framework.admin.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 平台用户 Mapper 接口
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

}
