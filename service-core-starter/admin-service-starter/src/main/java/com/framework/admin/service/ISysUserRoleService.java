package com.framework.admin.service;

import com.framework.admin.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色配置 服务类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
