package com.framework.admin.service.impl;

import com.framework.admin.entity.Config;
import com.framework.admin.mapper.ConfigMapper;
import com.framework.admin.service.IConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 平台配置信息 服务实现类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Service
public class ConfigServiceImpl extends ServiceImpl<ConfigMapper, Config> implements IConfigService {

}
