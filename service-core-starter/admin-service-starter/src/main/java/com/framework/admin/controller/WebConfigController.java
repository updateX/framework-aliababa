package com.framework.admin.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import com.framework.common.base.BaseController;

/**
 * <p>
 * 网站配置信息 前端控制器
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Controller
@RequestMapping("/admin/web-config")
public class WebConfigController extends BaseController {

}
