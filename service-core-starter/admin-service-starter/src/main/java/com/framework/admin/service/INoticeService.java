package com.framework.admin.service;

import com.framework.admin.entity.Notice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统资讯公告信息 服务类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
public interface INoticeService extends IService<Notice> {

}
