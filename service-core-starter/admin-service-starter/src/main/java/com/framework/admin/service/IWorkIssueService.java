package com.framework.admin.service;

import com.framework.admin.entity.WorkIssue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 工单记录 服务类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
public interface IWorkIssueService extends IService<WorkIssue> {

}
