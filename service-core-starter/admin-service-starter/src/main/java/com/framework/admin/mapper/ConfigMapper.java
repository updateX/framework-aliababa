package com.framework.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.framework.admin.entity.Config;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 平台配置信息 Mapper 接口
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Mapper
public interface ConfigMapper extends BaseMapper<Config> {

}
