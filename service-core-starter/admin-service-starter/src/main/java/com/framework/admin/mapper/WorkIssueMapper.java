package com.framework.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.framework.admin.entity.WorkIssue;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 工单记录 Mapper 接口
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Mapper
public interface WorkIssueMapper extends BaseMapper<WorkIssue> {

}
