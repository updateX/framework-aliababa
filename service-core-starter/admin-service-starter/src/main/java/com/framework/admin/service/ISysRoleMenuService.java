package com.framework.admin.service;

import com.framework.admin.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色菜单 服务类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

}
