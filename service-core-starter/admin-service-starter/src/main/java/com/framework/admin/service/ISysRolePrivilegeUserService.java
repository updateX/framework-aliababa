package com.framework.admin.service;

import com.framework.admin.entity.SysRolePrivilegeUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户权限配置 服务类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
public interface ISysRolePrivilegeUserService extends IService<SysRolePrivilegeUser> {

}
