package com.framework.admin.service.impl;

import com.framework.admin.entity.SysPrivilege;
import com.framework.admin.mapper.SysPrivilegeMapper;
import com.framework.admin.service.ISysPrivilegeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限配置 服务实现类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Service
public class SysPrivilegeServiceImpl extends ServiceImpl<SysPrivilegeMapper, SysPrivilege> implements ISysPrivilegeService {

}
