package com.framework.admin.service.impl;

import com.framework.admin.entity.Notice;
import com.framework.admin.mapper.NoticeMapper;
import com.framework.admin.service.INoticeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统资讯公告信息 服务实现类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements INoticeService {

}
