package com.framework.admin.service;

import com.framework.admin.entity.SysUserLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统日志 服务类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
public interface ISysUserLogService extends IService<SysUserLog> {

}
