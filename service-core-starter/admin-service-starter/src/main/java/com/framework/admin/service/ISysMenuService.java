package com.framework.admin.service;

import com.framework.admin.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统菜单 服务类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
public interface ISysMenuService extends IService<SysMenu> {

}
