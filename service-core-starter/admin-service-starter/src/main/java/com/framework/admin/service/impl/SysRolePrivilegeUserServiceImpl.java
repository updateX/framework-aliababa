package com.framework.admin.service.impl;

import com.framework.admin.entity.SysRolePrivilegeUser;
import com.framework.admin.mapper.SysRolePrivilegeUserMapper;
import com.framework.admin.service.ISysRolePrivilegeUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户权限配置 服务实现类
 * </p>
 *
 * @author xie jianchu
 * @since 2021-06-28
 */
@Service
public class SysRolePrivilegeUserServiceImpl extends ServiceImpl<SysRolePrivilegeUserMapper, SysRolePrivilegeUser> implements ISysRolePrivilegeUserService {

}
