package com.framework.common.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.framework.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_menu")
public class SysMenu extends BaseEntity {
    private static final long serialVersionUID = 3879184051862631734L;

    private String parentId;

    private String name;

    private String css;

    private String url;

    private String path;

    private Integer sort;

    private Integer type;

    private Boolean hidden;

    /**
     * 请求的类型
     */
    private String pathMethod;

    @TableField(exist = false)
    private List<SysMenu> subMenus;

    @TableField(exist = false)
    private String roleId;

    @TableField(exist = false)
    private Set<String> menuIds;
}
