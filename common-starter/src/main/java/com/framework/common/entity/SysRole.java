package com.framework.common.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.framework.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_role")
public class SysRole extends BaseEntity {

    private static final long serialVersionUID = -524642193478387954L;

    private String code;

    private String name;

    @TableField(exist = false)
    private Long userId;
}
