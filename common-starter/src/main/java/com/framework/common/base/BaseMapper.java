package com.framework.common.base;

/**
 * Mapper父类，注意这个类不要让MP扫描到
 *
 * @param <T>
 */
public interface BaseMapper<T> extends com.baomidou.mybatisplus.core.mapper.BaseMapper<T> {
    //这里可以放一些公共的方法
}
